const express = require("express");
const authMiddleware = require("../middlewares/auth");

const Post = require("../models/Post");

const router = express.Router();

router.use(authMiddleware);

router.get("/", async (req, res) => {
  const { page = 1, limit = 9 } = req.query;
  try {
    // executar buca com os valores da página e limites da página 
    const posts = await Post.find({}).populate("user")
      .limit(limit * 1)
      .skip((page - 1) * limit)
      .exec();

    // Pegar o total de documentos no model post
    const count = await Post.countDocuments();

    // retornar resposta com posts, total de paginas, and qual pagina estou
    res.send({
      posts,
      totalPages: Math.ceil(count / limit),
      currentPage: page
    });
  } catch (err) {
    console.error(err.message);
  }
});

router.get("/:postId", async (req, res) => {
  try {
    const post = await Post.findById(req.params.postId).populate("user");

    res.send(post);
  } catch (err) {
    return res.status(400).send({ error: "Erro ao carregar o post " });
  }
});

router.post("/", async (req, res) => {
  try {
    const post = await Post.create({ ...req.body, user: req.userId });
    return res.send({ post });
  } catch (err) {
    return res.status(400).send({ error: "Erro ao criar novo post" });
  }
});

router.put("/:postId", async (req, res) => {
  res.send({ user: req.userId });
});

router.delete("/:postId", async (req, res) => {
  try {
    await Post.findByIdAndRemove(req.params.postId);

    res.send();
  } catch (err) {
    return res.status(400).send({ error: "Erro ao deletar o post " });
  }
});

module.exports = (app) => app.use("/posts", router);
