const mongoose = require("../../database");
const Schema = mongoose.Schema;

const PostSchema = new Schema({
  titulo: {
    type: String,
    require: true,
  },
  descricao: {
    type: String,
    require: true,
  },
  conteudo: {
    type: String,
    require: true,
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: "User",
    require: true,
  },
  data: {
    type: Date,
    default: Date.now,
  },
});

const Post = mongoose.model("Post", PostSchema);

module.exports = Post;
